﻿anb_generate_minority_courtier = {
	if = {
		limit = { 
			any_held_title = { title_province = { has_province_modifier = marble_dwarvish_city } } 
			NOT = { 
				any_courtier = { 
					culture = culture:marble_dwarvish
					count >= 3
				}
			}
		}
		create_character = {
			template = marble_dwarf_minority_character
			employer = this
			dynasty = none
		}
	}
	else_if = {
		limit = { 
			any_held_title = { title_province = { has_province_modifier = ruby_dwarvish_city } } 
			NOT = { 
				any_courtier = { 
					culture = culture:ruby_dwarvish
					count >= 3
				}
			}
		}
		create_character = {
			template = ruby_dwarf_minority_character
			employer = this
			dynasty = none
		}
	}
	else_if = {
		limit = { 
			any_held_title = { title_province = { has_province_modifier = moon_elvish_city } } 
			NOT = { 
				any_courtier = { 
					culture = culture:moon_elvish
					count >= 3
				}
			}
		}
		create_character = {
			template = moon_elf_minority_character
			employer = this
			dynasty = none
		}
	}
	else_if = {
		limit = {
			government_has_flag = government_is_republic
			capital_province = { anb_has_minority_modifier = yes }
			NOT = { anb_owner_of_minority = yes }
		}
		depose = yes
	}
}
anb_generate_planetouched =  {
	if = {
		limit = { 
			location = { geographical_region = world_sarhal }
			NOT = { has_planetouched_trait = yes }
		}
		random_list = {
			95 = { }
			5 = {
				modifier = {
					factor = 0.75
					OR = {
						has_trait = race_elf
						has_trait = race_dwarf
						has_trait = race_halfling
					}
				}
				modifier = {
					factor = 1.25
					location = { geographical_region = world_sarhal_salahad_akasik }
				}
				modifier = {
					factor = 1.1
					culture = {
						OR = {
							has_cultural_pillar = heritage_fangaulan
							has_cultural_pillar = heritage_akasi
						}
					}
				}
				random_list = {
					10 = { #fire
						random_list = {
							75 = {
								add_trait = planetouched_fire_1
							}
							20 = {
								add_trait = planetouched_fire_2
							}
							5 = {
								add_trait = planetouched_fire_3
							}
						}
					}
					10 = { #water
						random_list = {
							75 = {
								add_trait = planetouched_water_1
							}
							20 = {
								add_trait = planetouched_water_2
								}
							5 = {
								add_trait = planetouched_water_3
							}
						}
					}
					10 = { #earth
						random_list = {
							75 = {
								add_trait = planetouched_earth_1
							}
							20 = {
								add_trait = planetouched_earth_2
							}
							5 = {
								add_trait = planetouched_earth_3
							}
						}
					}
					10 = { #wind
						modifier = {
							factor = 5
							location = { geographical_region = world_sarhal_bulwar }
						}	
						random_list = {
							75 = {
								add_trait = planetouched_wind_1
							}
							20 = {
								add_trait = planetouched_wind_2
							}
							5 = {
								add_trait = planetouched_wind_3
							}
						}
					}
					10 = { #life
						random_list = {
							75 = {
								add_trait = planetouched_life_1
							}
							20 = {
								add_trait = planetouched_life_2
							}
							5 = {
								add_trait = planetouched_life_3
							}
						}
					}
					10 = { #shadow
						random_list = {
							75 = {
								add_trait = planetouched_shadow_1
							}
							20 = {
								add_trait = planetouched_shadow_2
							}
							5 = {
								add_trait = planetouched_shadow_3
							}
						}
					}
				}
			}
		}
	}
}
anb_assign_religious_flavor_traits = {
	if = {
		limit = { religion = religion:bulwari_sun_cults_religion }
		random_list = {
			5 = {}
			5 = { add_trait = lifestyle_gardener }
		}
	}
	else_if = {
		limit = {
			OR = {
				religion = religion:harpy_hunts_religion
				religion = religion:skaldhyrric_religion
			}
		}
		random_list = {
			5 = {}
			5 = { add_trait = poet }
		}
	}
}