﻿namespace = planetouched

planetouched.0001 = {
	hidden = yes
	immediate = {
		if = {
			limit = { location = { geographical_region = world_sarhal } }
			random_list = {
				95 = { }
				5 = {
					modifier = {
						factor = 0.75
						OR = {
							has_trait = race_elf
							has_trait = race_dwarf
							has_trait = race_halfling
						}
					}
					modifier = {
						factor = 1.25
						location = { geographical_region = world_sarhal_salahad_akasik }
					}
					modifier = {
						factor = 1.1
						culture = {
							OR = {
								has_cultural_pillar = heritage_fangaulan
								has_cultural_pillar = heritage_akasi
							}
						}
					}
					random_list = {
						10 = { #fire
							random_list = {
								75 = {
									add_trait = planetouched_fire_1
								}
								20 = {
									add_trait = planetouched_fire_2
								}
								5 = {
									add_trait = planetouched_fire_3
								}
							}
						}
						10 = { #water
							random_list = {
								75 = {
									add_trait = planetouched_water_1
								}
								20 = {
									add_trait = planetouched_water_2
								}
								5 = {
									add_trait = planetouched_water_3
								}
							}
						}
						10 = { #earth
							random_list = {
								75 = {
									add_trait = planetouched_earth_1
								}
								20 = {
									add_trait = planetouched_earth_2
								}
								5 = {
									add_trait = planetouched_earth_3
								}
							}
						}
						10 = { #wind
							modifier = {
								factor = 5
								location = { geographical_region = world_sarhal_bulwar }
							}	
							random_list = {
								75 = {
									add_trait = planetouched_wind_1
								}
								20 = {
									add_trait = planetouched_wind_2
								}
								5 = {
									add_trait = planetouched_wind_3
								}
							}
						}
						10 = { #life
							random_list = {
								75 = {
									add_trait = planetouched_life_1
								}
								20 = {
									add_trait = planetouched_life_2
								}
								5 = {
									add_trait = planetouched_life_3
								}
							}
						}
						10 = { #shadow
							random_list = {
								75 = {
									add_trait = planetouched_shadow_1
								}
								20 = {
									add_trait = planetouched_shadow_2
								}
								5 = {
									add_trait = planetouched_shadow_3
								}
							}
						}
					}
				}
			}
		}
	}
}
