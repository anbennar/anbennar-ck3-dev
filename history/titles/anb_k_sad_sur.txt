k_ekluzagnu = {
	1006.6.22 = {
		liege = e_bulwar
		holder = bulwari0004 # Bawar II szel-Barseen
	}
}

d_zansap = {
	1006.1.1 = {
		holder = bulwari0004 # Bawar II szel-Barseen
	}
}

c_zansap = {
	1000.1.1 = { change_development_level = 18 }
	1006.1.1 = {
		holder = bulwari0004 # Bawar II szel-Barseen
	}
}

c_surib = {
	1000.1.1 = { change_development_level = 16 }
}

c_zagnuhar = {
	1000.1.1 = { change_development_level = 5 }
}

c_betlibar = {
	1000.1.1 = { change_development_level = 5 }
}

c_elisumu = {
	1000.1.1 = { change_development_level = 5 }
}

c_elum_szel_bazi = {
	1000.1.1 = { change_development_level = 10 }
}

c_arfajazan = {
	1000.1.1 = { change_development_level = 12 }
}

c_ekluzagnu = {
	1000.1.1 = { change_development_level = 15 }
}

c_sad_uras = {
	1000.1.1 = { change_development_level = 12 }
}

c_harranbar = { 
	1000.1.1 = { change_development_level = 8 }
}

c_umulu = {
	1000.1.1 = { change_development_level = 8 }
}

c_sad_sur = {
	1000.1.1 = { change_development_level = 10 }
}

c_sihbaga = {
	1000.1.1 = { change_development_level = 12 }
}

c_rakutmu = {
	1000.1.1 = { change_development_level = 10 }
}

c_ardubar = {
	1000.1.1 = { change_development_level = 8 }
}

c_mibunasru = {
	1000.1.1 = { change_development_level = 8 }
}

c_kesudagka = {
	1000.1.1 = { change_development_level = 8 }
}

c_anzakalis = {
	1000.1.1 = { change_development_level = 8 }
}