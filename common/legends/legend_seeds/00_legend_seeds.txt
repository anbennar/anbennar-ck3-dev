﻿# Anbennar: commenting vanilla stuff

# All start dates
# king_arthur = { } # Anbennar

# carolingian = { } # Anbennar

# sons_of_david = { } # Anbennar

# the_wheelwright = { } # Anbennar

# vercingetorix = { } # Anbennar

# saman_khudah = { } # Anbennar

# bahram_gur = { } # Anbennar

# cadell = { } # Anbennar

# descendants_of_brahman = { } # Anbennar

# gothic_kings = { } # Anbennar

# sons_of_rurik = { } # Anbennar

holy_warrior = { # Generico Crusader legend
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		faith = {
			OR = {
				has_doctrine_parameter = great_holy_wars_active
				has_doctrine_parameter = great_holy_wars_active_if_reformed
			}
		}
		has_trait = crusader_king
	}
	is_valid = {
		has_trait = crusader_king
	}

	chronicle = holy_warrior
	chronicle_properties = {
		ancestor = root
		religion = root.religion
	}
}

holy_site = { # Holy Site
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		is_landed = yes
		faith = {
			any_holy_site = {
				OR = {
					county.holder = root
					county.holder.top_liege = root
				}
			}
		}
	}
	is_valid = {
		piety_level >= very_high_piety_level
	}

	chronicle = saintly_location
	chronicle_properties = {
		location = root.location
		religion = root.religion
		faith = root.faith
	}
}

# 867 start date
# el_cid = { } # Anbennar

# alfred_of_wessex = { } # Anbennar

# peasant_emperor = { } # Anbennar


# 1066 start date
# william_gellones = { } # Anbennar

# edward_the_martyr = { } # Anbennar

# yazdagird_iii = { } # Anbennar

# afrasiyab = { } # Anbennar

# sceafa = { } # Claims to be descended from Noah # Anbennar

# new_troy_london = { } # Anbennar

# corineus = { } # Anbennar

# scota = { } # Scots claiming ancestry from Ancient Egypt # Anbennar

# pyusawhti = { } # Burmese legend claiming ancestry from a dragon goddess # Anbennar

# menelik_i = { } # Supposed son of Solomon and the Queen of Sheba # Anbennar

# kings_of_semien = { } # Ancestry to the Danites of Israel, and mythical kings of Semien # Anbennar

# roman_heritage = { } # Erm, my family are *actually* Roman nobles erm # Anbennar

# hunnic_heritage = { } # MY nomadic people *totally* are the Huns guys # Anbennar

# premysl = { } # Descent from Premysl of Bohemia # Anbennar

# borjigin = { } # Alan Gua was impregnated by a ray of light # Anbennar

# naga_descent = { } # Kashmiri kings claim descent from Naga (divine snake people or "sneople" for short) # Anbennar

# hieros_gamos_skirnismal = { } # Fjolnir Yngling descended from Freyr and Gerdr # Anbennar

# shibi_chakravarti = { } # Chola claiming descent from the legendary king Shibi # Anbennar

# the_red_hand = { } # He who first lays hand on Ulster shall be its King # Anbennar

# ragnarr = { } # Ol Raggy S # Anbennar

#Dynasty perk
ce1_heroic_legacy_1 = {
	type = legitimizing
	quality = famed
	is_shown = {
		dynasty = {
			has_dynasty_perk = ce1_heroic_legacy_1
			NOT = {
				exists = var:dynasty_legend_used
			}
		}
	}
	is_valid = {
		is_landed = yes
		OR = {
			is_ai = no
			# Prevent the AI from sniping the seed from the player
			AND = {
				is_ai = yes
				NOT = {
					dynasty = { any_dynasty_member = { is_ai = no } }
				}
			}
		}
	}
	chronicle = great_deed_dynasty
	chronicle_properties = {
		founder = house.house_founder
		dynasty = dynasty
		title = primary_title
	}
}
