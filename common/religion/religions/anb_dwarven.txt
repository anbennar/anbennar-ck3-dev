﻿dwarven_religion = {
	family = rf_dwarven

	doctrine = dwarven_hostility_doctrine
	
	doctrine = special_faith_regent_court_tolerance #So Rubyhold and Khugdihr are on good terms with neighboring human realms
	doctrine = special_faith_sun_cult_tolerance #So Ovdal Tungr and the Seghbandal are on good terms with neighboring human realms

	pagan_roots = yes

	#Main Group
	doctrine = doctrine_no_head
	doctrine = doctrine_gender_male_dominated
	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_crime
	doctrine = doctrine_adultery_men_accepted
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_close_kin_crime
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_shunned

	#Clerical Functions
	doctrine = doctrine_clerical_function_alms_and_pacification
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_spiritual_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_cremation

	traits = {
		virtues = { brave honest vengeful }
		sins = { craven deceitful forgiving }
	}

	custom_faith_icons = { #todo
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = { #todo?
		{ name = holy_order_guardians_of_divinerealm }
		{ name = holy_order_faithful_of_highgod }
		{ name = holy_order_warriors_of_the_symbol }
	}

	holy_order_maa = { dwarven_guardians }

	localization = {
		HighGodName = dwarven_high_god_name #Dagrin
		HighGodName2 = dwarven_high_god_name
		HighGodNamePossessive = dwarven_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = dwarven_high_god_name_alternate #The Everbeard
		HighGodNameAlternatePossessive = dwarven_high_god_name_alternate_possessive

		#Creator
		CreatorName = dwarven_creator_god_name #Halanna
		CreatorNamePossessive = dwarven_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_SHE
		CreatorHerHis = CHARACTER_HERHIS_HER
		CreatorHerHim = CHARACTER_HERHIM_HER

		#HealthGod
		HealthGodName = dwarven_health_god_name #Margurz
		HealthGodNamePossessive = dwarven_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = dwarven_fertility_god_name #Thyrfen
		FertilityGodNamePossessive = dwarven_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = dwarven_wealth_god_name #Miradeth
		WealthGodNamePossessive = dwarven_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_SHE
		WealthGodHerHis = CHARACTER_HERHIS_HER
		WealthGodHerHim = CHARACTER_HERHIM_HER

		#HouseholdGod
		HouseholdGodName = dwarven_household_god_name #Azmirethazin
		HouseholdGodNamePossessive = dwarven_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = dwarven_fate_god_name #Anvelind
		FateGodNamePossessive = dwarven_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_SHE
		FateGodHerHis = CHARACTER_HERHIS_HER
		FateGodHerHim = CHARACTER_HERHIM_HER

		#KnowledgeGod
		KnowledgeGodName = dwarven_knowledge_god_name #Urisazirn
		KnowledgeGodNamePossessive = dwarven_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod
		WarGodName = dwarven_war_god_name #Argezenna
		WarGodNamePossessive = dwarven_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_SHE
		WarGodHerHis = CHARACTER_HERHIS_HER
		WarGodHerHim = CHARACTER_HERHIM_HER

		#TricksterGod
		TricksterGodName = dwarven_trickster_god_name #Grobilazk
		TricksterGodNamePossessive = dwarven_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = dwarven_night_god_name #Jotzralanna
		NightGodNamePossessive = dwarven_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = dwarven_water_god_name #Uthojan
		WaterGodNamePossessive = dwarven_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM


		PantheonTerm = religion_the_gods
		PantheonTerm2 = religion_the_gods_2
		PantheonTerm3 = religion_the_gods_3
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = { 
			dwarven_high_god_name 
			dwarven_high_god_name_alternate
			dwarven_creator_god_name
			dwarven_health_god_name
			dwarven_fertility_god_name
			dwarven_wealth_god_name
			dwarven_household_god_name
			dwarven_fate_god_name
			dwarven_knowledge_god_name
			dwarven_war_god_name
			dwarven_night_god_name
			dwarven_water_god_name
			good_god_name_aesanna
			good_god_name_buran
			good_god_name_gurthbael
			good_god_name_setzovar
			good_god_name_durvorazir
			good_god_name_auirikus
			good_god_name_dolurazan
			good_god_name_derzobrazan
			good_god_name_bervinazan
			good_god_name_karazlov
			good_god_name_grimthar
			good_god_name_verdrik
			good_god_name_werdun
			good_god_name_lorgrim
			dwarven_death_deity_name
		}
		DevilName = dwarven_devil_name #The Void
		DevilNamePossessive = dwarven_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_IT
		DevilHerHis = CHARACTER_HERHIS_ITS
		DevilHerselfHimself = dwarven_devil_herselfhimself
		EvilGodNames = { 
			dwarven_devil_name
			dwarven_trickster_god_name
			evil_god_name_devils
			evil_god_name_astral_terrors
		}
		HouseOfWorship = cannorian_pantheon_house_of_worship #temple
		HouseOfWorship2 = cannorian_pantheon_house_of_worship
		HouseOfWorship3 = cannorian_pantheon_house_of_worship
		HouseOfWorshipPlural = cannorian_pantheon_house_of_worship_plural
		ReligiousSymbol = dwarven_religious_symbol #staff, i.e of Joztralanna
		ReligiousSymbol2 = dwarven_religious_symbol
		ReligiousSymbol3 = dwarven_religious_symbol
		ReligiousText = dwarven_religious_text #Book of Dagrin, Anvelind Codices
		ReligiousText2 = dwarven_religious_text
		ReligiousText3 = dwarven_religious_text
		ReligiousHeadName = cannorian_pantheon_religious_head_title #High Priest
		ReligiousHeadTitleName = cannorian_pantheon_religious_head_title_name #High Priesthood
		DevoteeMale = cannorian_pantheon_devotee_male #cleric
		DevoteeMalePlural = cannorian_pantheon_devotee_male_plural
		DevoteeFemale = cannorian_pantheon_devotee_female #cleric
		DevoteeFemalePlural = cannorian_pantheon_devotee_female_plural
		DevoteeNeuter = cannorian_pantheon_devotee_neuter #cleric
		DevoteeNeuterPlural = cannorian_pantheon_devotee_neuter_plural
		PriestMale = dwarven_pantheon_priest #hearth priest
		PriestMalePlural = dwarven_pantheon_priest_plural
		PriestFemale = dwarven_pantheon_priest_female #hearth priestess
		PriestFemalePlural = dwarven_pantheon_priest_female_plural
		PriestNeuter = dwarven_pantheon_priest #hearth priest
		PriestNeuterPlural = dwarven_pantheon_priest_plural
		AltPriestTermPlural = dwarven_pantheon_priest_term_plural #Hearth Priests
		BishopMale = dwarven_pantheon_bishop #high priest
		BishopMalePlural = cannorian_pantheon_bishop_plural
		BishopFemale = cannorian_pantheon_bishop_female #high priestess
		BishopFemalePlural = cannorian_pantheon_bishop_female_plural
		BishopNeuter = cannorian_pantheon_bishop #high_priest
		BishopNeuterPlural = cannorian_pantheon_bishop_plural
		DivineRealm = dwarven_divine_realm #Halls of Dagrin, Hall of the Gods, The Divine Forge
		DivineRealm2 = dwarven_divine_realm
		DivineRealm3 = dwarven_divine_realm
		PositiveAfterLife = dwarven_positive_afterlife #Under-Halls, The Divine Forge
		PositiveAfterLife2 = dwarven_positive_afterlife
		PositiveAfterLife3 = dwarven_positive_afterlife
		NegativeAfterLife = dwarven_negative_afterlife #The Drenches, The Void
		NegativeAfterLife2 = dwarven_negative_afterlife
		NegativeAfterLife3 = dwarven_negative_afterlife
		DeathDeityName = dwarven_death_deity_name #Lorgram
		DeathDeityNamePossessive = dwarven_death_deity_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		DeathDeityHerHim = CHARACTER_HERHIM_HIM
		
		GHWName = ghw_great_holy_war
		GHWNamePlural = ghw_great_holy_wars
	}	

	faiths = {
		ancestor_worship = {
			#Main Group
			color = { 99 99 99 }
			icon = ancestor_worship
			
			#All temporary, will be changed to actual holy sites when Dwarovar added
			holy_site = seghdihr
			holy_site = ovdal_tungr
			holy_site = khugdihr
			
			#holy_site = amldihr
			#holy_site = gronstunad
			#holy_site = ovdal_az_an
			#holy_site = hehodovar
			#holy_site = hul_jorkad
			#holy_site = arg_ordstun
			#holy_site = earthseed
			
			doctrine = tenet_teachings_of_everbeard	#protect the dwarovar!
			doctrine = tenet_children_of_halanna	#mountains are cool
			doctrine = tenet_ancestor_worship	#literaly it's name
			
			#Main Group
			doctrine = doctrine_gender_equal
			doctrine = doctrine_pluralism_pluralistic

			#Marriage
			doctrine = doctrine_consanguinity_restricted

			#Crimes
			doctrine = doctrine_homosexuality_shunned
			doctrine = doctrine_adultery_men_shunned
			doctrine = doctrine_witchcraft_crime

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			doctrine = doctrine_clerical_marriage_disallowed
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment
			
			localization = {
				#HealthGod
				HealthGodName = ancestor_worship_health_god_name #Muiri
				HealthGodNamePossessive = ancestor_worship_health_god_name_possessive
				HealthGodSheHe = CHARACTER_SHEHE_HE
				HealthGodHerHis = CHARACTER_HERHIS_HIS
				HealthGodHerHim = CHARACTER_HERHIM_HIM
				
				#FertilityGod
				FertilityGodName = ancestor_worship_fertility_god_name #Quila
				FertilityGodNamePossessive = ancestor_worship_fertility_god_name_possessive
				FertilityGodSheHe = CHARACTER_SHEHE_SHE
				FertilityGodHerHis = CHARACTER_HERHIS_HER
				FertilityGodHerHim = CHARACTER_HERHIM_HER

				#WealthGod
				WealthGodName = ancestor_worship_wealth_god_name #Urist
				WealthGodNamePossessive = ancestor_worship_wealth_god_name_possessive
				WealthGodSheHe = CHARACTER_SHEHE_HE
				WealthGodHerHis = CHARACTER_HERHIS_HIS
				WealthGodHerHim = CHARACTER_HERHIM_HIM

				#HouseholdGod
				HouseholdGodName = ancestor_worship_household_god_name #Hehogrim
				HouseholdGodNamePossessive = ancestor_worship_household_god_name_possessive
				HouseholdGodSheHe = CHARACTER_SHEHE_HE
				HouseholdGodHerHis = CHARACTER_HERHIS_HIS
				HouseholdGodHerHim = CHARACTER_HERHIM_HIM

				#FateGod
				FateGodName = ancestor_worship_fate_god_name #Gimron
				FateGodNamePossessive = ancestor_worship_fate_god_name_possessive
				FateGodSheHe = CHARACTER_SHEHE_HE
				FateGodHerHis = CHARACTER_HERHIS_HIS
				FateGodHerHim = CHARACTER_HERHIM_HIM

				#KnowledgeGod
				KnowledgeGodName = ancestor_worship_knowledge_god_name #Mellegar
				KnowledgeGodNamePossessive = ancestor_worship_knowledge_god_name_possessive
				KnowledgeGodSheHe = CHARACTER_SHEHE_HE
				KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
				KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

				#WarGod
				WarGodName = ancestor_worship_war_god_name #Krummul
				WarGodNamePossessive = ancestor_worship_war_god_name_possessive
				WarGodSheHe = CHARACTER_SHEHE_HE
				WarGodHerHis = CHARACTER_HERHIS_HIS
				WarGodHerHim = CHARACTER_HERHIM_HIM


				PantheonTerm = religion_the_ancestors
				GoodGodNames = { 
					dwarven_high_god_name 
					dwarven_high_god_name_alternate
					dwarven_creator_god_name
					ancestor_worship_health_god_name
					ancestor_worship_fertility_god_name
					ancestor_worship_wealth_god_name
					ancestor_worship_household_god_name
					ancestor_worship_fate_god_name
					ancestor_worship_knowledge_god_name
					ancestor_worship_war_god_name
					dwarven_night_god_name
					dwarven_water_god_name
					deity_balgar
					paganism_good_god_ancestors
				}
			}
		}
		dwarven_pantheon = {
			#Main Group
			color = { 58 0 15 }
			icon = dwarven_pantheon
			religious_head = d_halanna
			
			#All temporary, will be changed to actual holy sites when Dwarovar added
			holy_site = rubyhold
			holy_site = khugdihr
			holy_site = verkal_gulan
			
			doctrine = tenet_teachings_of_everbeard	#protect the dwarovar!
			doctrine = tenet_children_of_halanna	#mountains are cool
			doctrine = tenet_bhakti
			
			#doctrine = special_doctrine_not_allowed_to_hof #No-one respects Ovdal-az-An's authority anymore due to them isolating themselves #TODO - Set this up properly after Ovdal-az-An added
		
			doctrine = doctrine_spiritual_head #TODO - Change to temporal head so granite dwarves are playable
			doctrine = doctrine_theocracy_temporal #TODO - Change to lay clergy so granite dwarves are playable
			doctrine = doctrine_clerical_succession_spiritual_appointment #TODO - Change to temporal appointment so granite dwarves are playable
		}
	}
}