﻿# House of Wystanning
# House of Lanwic
# House of Forkwic
# House of Grannvale
# House of Humacsael
# House of Ancelor
# House of Wallor
# House of Treomar
# House of Vere
# House of Meregard
# Lowborns

##############
# Wystanning #
##############

wystanning_0001 = {
	name = Ricard
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_intrigue_3
	trait = patient 
	trait = wrathful
	trait = arrogant
	trait = scarred
	trait = race_human

	father = wystanning_0003
	mother = lanwic_0006

	diplomacy = 2
	martial = 5
	stewardship = 2
	intrigue = 7
	learning = 3

	998.1.1 = {
		birth = yes
	}
}

wystanning_0002 = {
	name = Trystan
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_diplomacy_2
	trait = calm 
	trait = ambitious
	trait = impatient
	trait = race_human

	father = wystanning_0003
	mother = lanwic_0006

	1000.3.11 = {
		birth = yes
	}
}

wystanning_0003 = {
	name = Ricard
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_diplomacy_2
	trait = calm 
	trait = ambitious
	trait = impatient
	trait = race_human

	father = wystanning_0007

	980.9.14 = {
		birth = yes
	}
	997.12.31 = {
		add_spouse = lanwic_0006
	}
	1013.1.1 = {
		death = {
			death_reason = death_battle
		}
	}
}
wystanning_0004 = {
	name = Venac
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_diplomacy_2
	trait = deceitful 
	trait = ambitious
	trait = patient
	trait = race_human

	diplomacy = 3
	martial = 4
	stewardship = 3
	intrigue = 7
	learning = 3

	father = wystanning_0007

	985.1.21 = {
		birth = yes
	}
	1001.3.1 = {
		add_spouse = humacfeld_0001 # Varina Lowborn
	}
}
wystanning_0005 = {
	name = Acara
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_learning_3
	trait = generous 
	trait = ambitious
	trait = impatient
	trait = race_human

	father = wystanning_0004
	mother = humacfeld_0001

	1006.1.1 = {
		birth = yes
	}
}
wystanning_0006 = {
	name = Corac
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite

	trait = curious
	trait = intellect_bad_1
	trait = humble

	father = wystanning_0004
	mother = humacfeld_0001

	1016.1.1 = {
		birth = yes
	}
}
wystanning_0007 = {
	name = Erlac
	dynasty = dynasty_wystanning
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_2
	trait = craven 
	trait = stubborn
	trait = gregarious
	trait = race_human

	945.1.1 = {
		birth = yes
	}
	992.1.1 = {
		death = yes
	}
}

##########
# Lanwic #
##########

lanwic_0001 = {
	name = Carleon
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_3
	trait = vengeful 
	trait = ambitious
	trait = chaste
	trait = depressed_1
	trait = race_human
	trait = twin

	father = lanwic_0003

	diplomacy = 4
	martial = 7
	stewardship = 2
	intrigue = 4
	learning = 0

	1005.2.2  = {
		birth = yes
	}
	1014.2.16 = {
		effect = {
			set_relation_grudge = character:wystanning_0001 # Ricard
			set_relation_grudge = character:wystanning_0004 # Venac
		}		
	}
}
lanwic_0002 = {
	name = Sofina
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_martial_2
	trait = gregarious 
	trait = generous
	trait = stubborn
	trait = race_human
	trait = twin

	father = lanwic_0003

	1005.2.2  = {
		birth = yes
	}
}
lanwic_0003 = {
	name = Arctur
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_3
	trait = compassionate
	trait = gregarious
	trait = fickle
	trait = race_human

	father = lanwic_0007

	979.11.20  = {
		birth = yes
	}
	1014.2.16 = {
		death = {
			death_reason = death_battle
		}
	}
}
lanwic_0004 = {
	name = Ardac
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_2
	trait = cynical
	trait = trusting
	trait = arrogant
	trait = race_human

	father = lanwic_0007

	980.5.14 = {
		birth = yes
	}
	1014.2.16 = {
		death = {
			death_reason = death_battle
		}
	}
}
lanwic_0005 = {
	name = Alos
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_intrigue_2
	trait = ambitious
	trait = deceitful
	trait = sadistic
	trait = bastard
	trait = race_human

	father = lanwic_0004

	999.6.6  = {
		birth = yes
	}
}
lanwic_0006 = {
	name = Valenca
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_intrigue_3
	trait = ambitious
	trait = deceitful
	trait = arrogant
	trait = race_human

	father = lanwic_0007

	981.7.16  = {
		birth = yes
	}
}
lanwic_0007 = {
	name = Ulric
	dynasty = dynasty_lanwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_intrigue_3
	trait = trusting
	trait = patient
	trait = gluttonous
	trait = race_human

	950.1.11  = {
		birth = yes
	}
	980.1.1  = {
		death = {
			death_reason = death_battle
		}
	}
}

###########
# Forkwic #
###########

forkwic_0001 = {
	name = Brien
	dynasty = dynasty_forkwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_4
	trait = arrogant
	trait = ambitious
	trait = greedy
	trait = race_human

	diplomacy = 4
	martial = 2
	stewardship = 7
	intrigue = 0
	learning = 4

	975.3.12  = {
		birth = yes
	}
}
forkwic_0002 = {
	name = Humac
	dynasty = dynasty_forkwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_2
	trait = arrogant
	trait = impatient
	trait = stubborn
	trait = race_human
	father = forkwic_0001

	995.2.19 = {
		birth = yes
	}
	1012.1.31 = {
		add_spouse = humacfeld_0002
	}
}
forkwic_0003 = {
	name = Marcan
	dynasty = dynasty_forkwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = charming
	trait = patient
	trait = race_human
	father = forkwic_0002

	1012.9.31  = {
		birth = yes
	}
}
forkwic_0004 = {
	name = Riwan
	dynasty = dynasty_forkwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = curious
	trait = compassionate
	trait = race_human
	father = forkwic_0002

	1013.7.31 = {
		birth = yes
	}
}
forkwic_0005 = {
	name = Elecast
	dynasty = dynasty_forkwic
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_learning_3
	trait = shy
	trait = honest
	trait = brave
	trait = race_human
	father = forkwic_0001

	997.9.11 = {
		birth = yes
	}
}
forkwic_0006 = {
	name = Edith
	dynasty = dynasty_forkwic
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_learning_3
	trait = compassionate
	trait = stubborn
	trait = chaste
	trait = race_human
	father = forkwic_0001

	1000.10.2 = {
		birth = yes
	}
}

#############
# Grannvale #
#############

grannvale_0001 = {
	name = Edmund
	dynasty = dynasty_grannvale
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_4
	trait = lustful
	trait = greedy
	trait = ambitious
	trait = peasant_leader
	trait = race_human

	diplomacy = 4
	martial = 10
	stewardship = 2
	intrigue = 1
	learning = 2

	1000.5.11  = {
		birth = yes
	}
	1017.2.19 = {
		add_spouse = humacfeld_0003
	}
}
grannvale_0002 = {
	name = Bella
	dynasty = dynasty_grannvale
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = charming
	trait = impatient
	trait = bastard
	trait = race_human
	father = grannvale_0001

	1017.2.2  = {
		birth = yes
	}
}
grannvale_0003 = {
	name = Lisolene
	dynasty = dynasty_grannvale
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = pensive
	trait = compassionate
	trait = race_human
	father = grannvale_0001
	mother = humacfeld_0003

	1019.1.1  = {
		birth = yes
	}
}
grannvale_0004 = {
	name = Rabac
	dynasty = dynasty_grannvale
	religion = castanorian_pantheon
	culture = trystanite
	
	trait = race_human
	father = grannvale_0001
	mother = humacfeld_0003

	1020.12.12  = {
		birth = yes
	}
}
grannvale_0005 = {
	name = Lan
	dynasty = dynasty_grannvale
	religion = castanorian_pantheon
	culture = trystanite
	
	trait = race_human
	father = grannvale_0001
	mother = humacfeld_0003

	1021.6.13  = {
		birth = yes
	}
}

#############
# Humacsael #
#############

humacsael_0001 = {
	name = Vencan
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_3
	trait = gregarious
	trait = ambitious
	trait = compassionate
	trait = race_human

	father = humacsael_0006

	diplomacy = 2
	martial = 7
	stewardship = 2
	intrigue = 4
	learning = 2

	980.1.1 = {
		birth = yes
	}
	986.1.1 = {
		set_primary_title_to = title:c_humacs_rest
	}
	1016.1.1 = {
		trait = maimed
	}
}
humacsael_0002 = {
	name = Acromar
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_3
	trait = arrogant
	trait = impatient
	trait = lazy
	trait = race_human

	father = humacsael_0006

	983.2.7 = {
		birth = yes
	}
}
humacsael_0003 = {
	name = Etta
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_learning_3
	trait = fickle
	trait = impatient
	trait = sadistic
	trait = race_human

	father = humacsael_0006

	992.7.1 = {
		birth = yes
	}
}
humacsael_0004 = {
	name = Coreg
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_1
	trait = callous
	trait = humble
	trait = greedy
	trait = race_human

	father = humacsael_0006

	993.6.4 = {
		birth = yes
	}
	1001.1.14 = {
		effect = {
			set_relation_friend = character:humacsael_0005
		}
	}
}
humacsael_0005 = {
	name = Stovac
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_2
	trait = cynical
	trait = stubborn
	trait = arrogant
	trait = race_human

	father = humacsael_0001
	mother = humacfeld_0007

	996.1.14 = {
		birth = yes
	}
}
humacsael_0006 = {
	name = Peyter
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_intrigue_2
	trait = race_human

	father = humacsael_0007

	960.3.1 = {
		birth = yes
	}
	1010.3.14 = {
		death = {
			death_reason = death_ill
		}
	}
}
humacsael_0007 = {
	name = Tomar
	dynasty = dynasty_humacsael
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_diplomacy_2
	trait = race_human

	940.4.21 = {
		birth = yes
	}
	980.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}

############
# Ancrelor #
############

ancrelor_0001 = {
	name = Dostan
	dynasty = dynasty_ancrelor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_2
	trait = callous 
	trait = diligent
	trait = wrathful
	trait = scarred
	trait = peasant_leader
	trait = race_human

	father = ancrelor_0003

	diplomacy = 2
	martial = 2
	stewardship = 7
	intrigue = 4
	learning = 3

	1001.8.14 = {
		birth = yes
	}
}
ancrelor_0002 = {
	name = Acromar
	dynasty = dynasty_ancrelor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_3
	trait = forgiving 
	trait = humble
	trait = temperate
	trait = race_human

	father = ancrelor_0003

	1002.1.19 = {
		birth = yes
	}
}
ancrelor_0003 = {
	name = Ancrel
	dynasty = dynasty_ancrelor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_4
	trait = gregarious 
	trait = stubborn
	trait = brave
	trait = shrewd
	trait = race_human

	970.6.1 = {
		birth = yes
	}
	1010.10.10 = {
		death = {
			death_reason = death_battle
		}
	}
}

##########
# Wallor #
##########

wallor_0001 = {
	name = Valerian
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_stewardship_2
	trait = craven
	trait = calm
	trait = honest
	trait = race_human

	father = wallor_0006

	diplomacy = 2
	martial = 2
	stewardship = 5
	intrigue = 3
	learning = 3

	990.11.9  = {
		birth = yes
	}
}
wallor_0002 = {
	name = Bellac
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_diplomacy_2
	trait = temperate
	trait = fickle
	trait = paranoid
	trait = race_human

	father = wallor_0006

	diplomacy = 5
	martial = 2
	stewardship = 2
	intrigue = 3
	learning = 3

	991.10.31 = {
		birth = yes
	}
}
wallor_0003 = {
	name = Wyllamina
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_diplomacy_1
	trait = race_human

	father = wallor_0006

	992.4.2  = {
		birth = yes
	}
}
wallor_0004 = {
	name = Trystan
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_diplomacy_1
	trait = lazy
	trait = content
	trait = lustful
	trait = rakish
	trait = race_human

	father = wallor_0006

	993.9.12  = {
		birth = yes
	}
}
wallor_0005 = {
	name = Madalac
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian

	trait = charming
	trait = gregarious
	trait = bastard
	trait = race_human

	father = wallor_0004

	1010.5.1  = {
		birth = yes
	}
}
wallor_0006 = {
	name = Evin
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_martial_4
	trait = gregarious
	trait = stubborn
	trait = brave
	trait = shrewd
	trait = race_human

	father = wallor_0008

	970.8.13 = {
		birth = yes
	}
	1019.10.10 = {
		death = yes
	}
}
wallor_0007 = {
	name = Madala
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_diplomacy_3
	trait = diligent
	trait = arrogant
	trait = zealous
	trait = race_human

	father = wallor_0008

	975.6.8 = {
		birth = yes
	}
}
wallor_0008 = {
	name = Delan
	dynasty = dynasty_wallor
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_martial_1
	trait = ambitious
	trait = stubborn
	trait = greedy
	trait = intellect_bad_1
	trait = race_human

	950.1.1 = {
		birth = yes
	}
	1003.5.5 = {
		death = yes
	}
}
####################
# House of Treomar #
####################
treomar_0001 = {
	name = Castan
	dynasty = dynasty_treomar
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_stewardship_2
	trait = just
	trait = ambitious
	trait = craven
	trait = race_human
	trait = depressed_1

	father = treomar_0005

	diplomacy = 2
	martial = 2
	stewardship = 5
	intrigue = 3
	learning = 4

	976.9.9  = {
		birth = yes
	}
	992.11.15 = {
		add_spouse = wallor_0007
	}
}
treomar_0002 = {
	name = Alina
	dynasty = dynasty_treomar
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_diplomacy_1
	trait = zealous
	trait = trusting
	trait = compassionate
	trait = race_human

	father = treomar_0001
	mother = wallor_0007

	diplomacy = 2
	martial = 2
	stewardship = 5
	intrigue = 3
	learning = 4

	1000.7.12  = {
		birth = yes
	}
}
treomar_0003 = {
	name = Gelmac
	dynasty = dynasty_treomar
	religion = castanorian_pantheon
	culture = castanorian

	trait = charming
	trait = bastard
	trait = race_half_elf

	father = treomar_0001

	diplomacy = 2
	martial = 2
	stewardship = 5
	intrigue = 3
	learning = 4

	1020.3.1  = {
		birth = yes
	}
}
treomar_0004 = {
	name = Castian
	dynasty = dynasty_treomar
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_martial_1
	trait = race_human

	father = treomar_0001
	mother = wallor_0007

	996.1.1 = {
		birth = yes
	}
	1014.5.14 = {
		death = {
			death_reason = death_battle_of_morban_flats
		}
	}
}
treomar_0005 = {
	name = Frederic
	dynasty = dynasty_treomar
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_learning_2
	trait = race_human

	951.1.1  = {
		birth = yes
	}
	1007.1.1 = {
		death = yes
	}
}
########
# Vere #
########

vere_0001 = {
	name = Venall
	dynasty = dynasty_vere
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_stewardship_2
	trait = honest
	trait = humble
	trait = eccentric
	trait = race_human

	father = vere_0006

	diplomacy = 7
	martial = 2
	stewardship = 0
	intrigue = 3
	learning = 1

	977.1.1  = {
		birth = yes
	}
	996.1.1 = {
		add_spouse = humacfeld_0004
	}
}
vere_0002 = {
	name = Alfred
	dynasty = dynasty_vere
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_intrigue_2
	trait = fickle
	trait = arrogant
	trait = zealous
	trait = race_human

	father = vere_0006

	978.2.15  = {
		birth = yes
	}
	995.1.1 = {
		add_spouse = humacfeld_0005
	}
}
vere_0003 = {
	name = Cora
	dynasty = dynasty_vere
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_diplomacy_4 
	trait = diligent
	trait = ambitious
	trait = generous
	trait = race_human

	father = vere_0001
	mother = humacfeld_0004

	997.12.15  = {
		birth = yes
	}
}
vere_0004 = {
	name = Elenor
	dynasty = dynasty_vere
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_diplomacy_2 
	trait = chaste
	trait = impatient
	trait = stubborn
	trait = beauty_good_1
	trait = race_human

	father = vere_0001
	mother = humacfeld_0004

	1000.6.16  = {
		birth = yes
	}
}
vere_0005 = {
	name = Robyn
	dynasty = dynasty_vere
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_martial_1 
	trait = callous
	trait = trusting
	trait = gluttonous
	trait = race_human

	father = vere_0002
	mother = humacfeld_0005

	1001.3.31  = {
		birth = yes
	}
}
vere_0006 = {
	name = Castian
	dynasty = dynasty_vere
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_stewardship_4  
	trait = race_human

	950.11.1  = {
		birth = yes
	}
	1000.8.1  = {
		death = yes
	}
}
############
# Meregard #
############

meregard_0001 = {
	name = Welyam
	dynasty = dynasty_meregard
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_martial_3
	trait = arrogant
	trait = ambitious
	trait = arbitrary
	trait = race_human

	father = meregard_0005

	diplomacy = 4
	martial = 7
	stewardship = 2
	intrigue = 2
	learning = 2

	1001.1.1  = {
		birth = yes
	}
	1017.1.1 = {
		add_spouse = humacfeld_0006
	}
}
meregard_0002 = {
	name = Dalya
	dynasty = dynasty_meregard
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_intrigue_2
	trait = race_human

	father = meregard_0005

	1002.10.31  = {
		birth = yes
	}
}
meregard_0003 = {
	name = Teacan
	dynasty = dynasty_meregard
	religion = castanorian_pantheon
	culture = castanorian

	trait = race_half_elf
	trait = twin

	father = meregard_0001
	mother = humacfeld_0006

	1021.12.15  = {
		birth = yes
	}
}
meregard_0004 = {
	name = Arcturian
	dynasty = dynasty_meregard
	religion = castanorian_pantheon
	culture = castanorian

	trait = race_half_elf
	trait = twin

	father = meregard_0001
	mother = humacfeld_0006

	1021.12.15  = {
		birth = yes
	}
}
meregard_0005 = {
	name = Teacan
	dynasty = dynasty_meregard
	religion = castanorian_pantheon
	culture = castanorian

	trait = education_stewardship_4
	trait = race_human

	967.1.1  = {
		birth = yes
	}
	1017.6.15 = {
		death = yes
	}
}

#############
# TRYSTANOR #
#############

# TODO 'Trystanor' as a house name can be improved

trystanor_0001 = {
	name = Trystan
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_4
	trait = calm 
	trait = ambitious
	trait = generous
	trait = race_human

	diplomacy = 5
	stewardship = 6
	martial = 10
	learning = 3
	intrigue = 2

	430.1.1 = {
		birth = yes
	}
	501.1.1 = {
		death = yes
	}
}
trystanor_0002 = {
	name = Castan
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_2
	trait = race_human
	father = trystanor_0001

	460.1.1 = {
		birth = yes
	}
	530.1.1 = {
		death = yes
	}
}
trystanor_0003 = {
	name = Osric
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_stewardship_2
	trait = race_human
	father = trystanor_0002

	490.1.1 = {
		birth = yes
	}
	535.1.1 = {
		death = yes
	}
}
trystanor_0004 = {
	name = Trystan
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_1
	trait = race_human
	father = trystanor_0003

	515.1.1 = {
		birth = yes
	}
	570.1.1 = {
		death = yes
	}
}
trystanor_0005 = {
	name = Trystan
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_3
	trait = race_human
	father = trystanor_0004

	531.1.1 = {
		birth = yes
	}
	550.1.1 = {
		death = {
			death_reason = death_battle
		}
	}
}
trystanor_0006 = {
	name = Castan
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_2
	trait = race_human
	father = trystanor_0004

	533.1.1 = {
		birth = yes
	}
	560.1.1 = {
		death = {
			death_reason = death_battle
		}
	}
}
trystanor_0007 = {
	name = Daynan
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite

	trait = education_martial_2
	trait = race_human
	father = trystanor_0004

	534.1.1 = {
		birth = yes
	}
	563.1.1 = {
		death = {
			death_reason = death_battle
		}
	}
}
trystanor_0008 = {
	name = Varina
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_learning_2
	trait = race_human
	father = trystanor_0003

	518.1.1 = {
		birth = yes
	}
	568.1.1 = {
		death = yes
	}
}
trystanor_0009 = {
	name = Mathilde
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_diplomacy_2
	trait = race_human
	father = trystanor_0003

	519.1.1 = {
		birth = yes
	}
	570.1.1 = {
		death = yes
	}
}
trystanor_0010 = {
	name = Edith
	dynasty = dynasty_trystanor
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_intrigue_2
	trait = race_human
	father = trystanor_0003

	520.1.1 = {
		birth = yes
	}
	569.1.1 = {
		death = yes
	}
}
############
# Lowborns #
############

humacfeld_0001 = {
	name = Varina
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_intrigue_1
	trait = calm 
	trait = stubborn
	trait = generous
	trait = race_human

	980.9.9 = {
		birth = yes
	}	
}
humacfeld_0002 = {
	name = Gicela
	religion = castanorian_pantheon
	culture = trystanite
	female = yes

	trait = education_learning_2
	trait = just 
	trait = lazy
	trait = shy
	trait = race_human

	996.1.4 = {
		birth = yes
	}	
}
humacfeld_0003 = {
	name = Constance
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_martial_2
	trait = brave
	trait = ambitious
	trait = greedy
	trait = race_human

	999.2.19 = {
		birth = yes
	}	
}
humacfeld_0004 = {
	name = Mariel
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_intrigue_2
	trait = sadistic
	trait = wrathful
	trait = temperate
	trait = race_human

	980.4.14 = {
		birth = yes
	}	
}
humacfeld_0005 = {
	name = Alice
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_stewardship_1
	trait = brave
	trait = trusting
	trait = humble
	trait = race_human

	979.1.2 = {
		birth = yes
	}	
}
humacfeld_0006 = {
	name = Adra
	religion = elven_forebears
	culture = moon_elvish
	female = yes

	trait = education_stewardship_2
	trait = lustful
	trait = content
	trait = honest
	trait = race_elf

	998.2.13 = {
		birth = yes
	}	
}
humacfeld_0007 = {
	name = Auci
	religion = castanorian_pantheon
	culture = castanorian
	female = yes

	trait = education_stewardship_2
	trait = race_human

	978.1.2 = {
		birth = yes
	}	
}