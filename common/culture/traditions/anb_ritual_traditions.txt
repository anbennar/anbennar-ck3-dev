﻿tradition_children_of_akasik = {
	category = ritual

	layers = {
		0 = learning
		1 = mediterranean
		4 = mountain.dds
	}

	is_shown = {
		OR = {
			has_cultural_pillar = heritage_akasi
			any_culture_county = {
				title_province = {
					geographical_region = world_sarhal_salahad_akasik
				}
			}
		}
	}
	can_pick = {
		custom_description = {
			text = culture_in_mountains_desc
			any_culture_county = {
				any_county_province = {
					OR = {
						terrain = mountains
						terrain = desert_mountains
					}
				}
			}
		}
	}
	
	parameters = {
		piety_gain_on_building_complete_in_mountains = yes
		#unlock_akasi_mountain_decision = yes #This needs to be made first
	}
	province_modifier = {
		desert_mountains_tax_mult = 0.05
	}
	county_modifier = {
		desert_mountains_development_growth_factor = 0.20
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOT = {
						any_culture_county = {
							percent >= 0.3
							any_county_province = {
								OR = {
									terrain = mountains
									terrain = desert_mountains
								}
							}
						}
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = mountain_percentage_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOT = {
					scope:character = {
						any_sub_realm_county = {
							percent >= 0.2
							culture = scope:character.culture
							any_county_province = {
								OR = {
									terrain = mountains
									terrain = desert_mountains
								}
							}
						}
					}
				}
			}
			multiply = 0
		}
		else_if = {
			limit = {
				any_culture_county = {
					percent >= 0.6
					any_county_province = {
						OR = {
							terrain = mountains
							terrain = desert_mountains
						}
					}
				}
			}
			multiply = 8
		}
	}
}
tradition_order_reverence = {
	category = ritual

	layers = {
		0 = learning
		1 = mediterranean
		4 = mountain.dds
	}

	is_shown = {
		OR = {
			has_cultural_pillar = heritage_akasi
			any_culture_county = {
				title_province = {
					geographical_region = world_sarhal_salahad_akasik
				}
			}
		}
	}
	can_pick = {
		scope:character = {
			faith = {
				has_doctrine_parameter = take_vows_active
			}
		}
	}
	
	parameters = {
		devoted_trait_bonuses = yes
		monastic_expectations = yes
		next_level_monasteries = yes
		prowess_from_religious_traits = yes
		culture_clergy_can_fight = yes
		monastery_prowess_martial_bonus = yes
		#unlock_maa_maddakas_markasi #This needs to be made first
	}
	character_modifier = {
		prowess_per_piety_level = 1
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			# Piety level requirement.
			if = {
				limit = {
					NOT = { scope:character.piety_level >= high_piety_level }
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = lacks_high_piety_level_desc
				}
			}
			if = {
				limit = {
					trigger_if = {
						limit = {
							scope:character = {
								is_ai = no
							}
						}
						NOT = {
							scope:character = {
								save_temporary_scope_as = culture_head_link
								faith = {
									any_faith_character = {
										count >= 15
										culture = scope:culture_head_link.culture
										prowess >= 10
										OR = {
											is_clergy = yes
											has_education_learning_trigger = yes
										}
									}
								}
							}
						}
					}
					trigger_else = {
						always = no
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = culture_head_faith_clergy_prowess_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					culture_pillar:ethos_bellicose = { is_in_list = traits }
					culture_pillar:ethos_spiritual = { is_in_list = traits }
				}
			}
			multiply = 0.25
		}
	}
}