﻿#Modifers for eidoueni religion (deity)

artanos_deity = {
	icon = martial_positive
	martial = 2
	prowess = 2
}
turanos_deity = {
	icon = martial_positive
	general_opinion = -20
	martial = 1
	diplomacy = 1
	stewardship = 1
	intrigue = 1
	learning = 1
}
sorbodua_deity = {
	icon = martial_positive
	intrigue = 1
	scheme_discovery_chance_mult = 0.05
	owned_hostile_scheme_success_chance_add = 5
}
dercanos_deity = {
	icon = martial_positive
	fertility = 0.1
	owned_personal_scheme_success_chance_add = 20
}
belouina_deity = {
	icon = martial_positive
	monthly_prestige = 0.5
	monthly_piety = 0.5
	stress_gain_mult = -0.1
	stress_loss_mult = 0.1
}
damarta_deity = {
	icon = martial_positive
	# magic = 3
}
asmirethin_deity = {
	icon = martial_positive
	build_gold_cost = -0.1
	build_speed = -0.1
}
careslobos_deity = {
	icon = martial_positive
	general_opinion = 10
}
trovecos_deity = {
	icon = martial_positive
	learning = 2
	health = 0.25
}
merisse_deity = {
	icon = martial_positive
	movement_speed = 0.1
	character_travel_speed_mult = 0.1
	naval_movement_speed_mult = 0.1
	development_growth = 0.1
}