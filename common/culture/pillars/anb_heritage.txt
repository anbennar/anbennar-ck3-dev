﻿
# Elves

heritage_elven = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_elven
		}
	}
	audio_parameter = european
}

# Dwarves

heritage_dwarven = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_dwarven
		}
	}
	audio_parameter = european
}

# Gnomes

heritage_gnomish = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_gnomish
		}
	}
	audio_parameter = european
}

# Halfling

heritage_halfling = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_halfling
		}
	}
	audio_parameter = european
}

# Harpies

heritage_harpy = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_harpy
		}
	}
	audio_parameter = mena
}

# Kobolds

heritage_kobold = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_kobold
		}
	}
	audio_parameter = european
}

# Gnolls

heritage_gnollish = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_gnollish
		}
	}
	audio_parameter = mena
}

# Humans

heritage_akasi = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_akasi
		}
	}
	audio_parameter = european
}

heritage_aldescanni = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_aldescanni
		}
	}
	audio_parameter = european
}

heritage_alenic = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_alenic
		}
	}
	audio_parameter = european
}

heritage_bulwari = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_bulwari
		}
	}
	audio_parameter = mena
}

heritage_businori = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_businori
		}
	}
	audio_parameter = european
}

heritage_cardesti = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_cardesti
		}
	}
	audio_parameter = mena
}

heritage_damesheader = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_damesheader
		}
	}
	audio_parameter = european
}

heritage_dostanorian = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_dostanorian
		}
	}
	audio_parameter = european
}

heritage_escanni = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_escanni
		}
	}
	audio_parameter = european
}

heritage_fangaulan = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_fangaulan
		}
	}
	audio_parameter = european
}

heritage_gerudian = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_gerudian
		}
	}
	audio_parameter = european
}

heritage_kheteratan = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_kheteratan
		}
	}
	audio_parameter = mena
}

heritage_lencori = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_lencori
		}
	}
	audio_parameter = european
}

heritage_rohibonic = {
	type = heritage
	is_shown = {
		heritage_is_shown_trigger = {
			HERITAGE = heritage_rohibonic
		}
	}
	audio_parameter = european
}
