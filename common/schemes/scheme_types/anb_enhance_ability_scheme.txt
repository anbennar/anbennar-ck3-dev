﻿
anb_enhance_ability_spell = {
	# Basic Setup
	skill = learning
	desc = anb_enhance_ability_desc_general
	success_desc = "anb_enhance_ability_success_desc"
	discovery_desc = "anb_enhance_ability_discovery_desc"
	icon = icon_scheme_fabricate_hook # TODO
	illustration = "gfx/interface/illustrations/event_scenes/corridor.dds" # TODO
	category = personal
	target_type = character
	is_secret = no
	uses_resistance = no
	cooldown = { years = 10 }
	
	# Parameters
	speed_per_skill_point = 8
	speed_per_target_skill_point = 0
	spymaster_speed_per_skill_point = 0
	target_spymaster_speed_per_skill_point = 0
	tier_speed = 1	
	base_progress_goal = 100
	maximum_secrecy = 95
	base_maximum_success = 100
	phases_per_agent_charge = 1
	success_chance_growth_per_skill_point = t2_scgpsp_value
	
	# Core Triggers
	allow = {
		is_adult = yes
		scope:target = {
			is_adult = yes
		}

		#Cannot cast if someone is already casting the same one on the target
		custom_description = {
			text = spell_already_being_cast_on
			subject = scope:target
			scope:target = {
				NOT = {
					any_targeting_scheme = {
						scheme_type = anb_enhance_ability_spell
					}
				}
			}
		}

		#Cannot cast if already been cast on the character in last 3 years
		NOT = { scope:target = { has_character_flag = recently_had_enhance_ability_attempt } }

		#Add cannot cast if someone already has an enhanced stat?

		#add 'knows spell' trigger later
	}

	valid = {
		NOT = { is_at_war_with = scope:target }
		scope:target = {
			OR = {
				exists = location
				in_diplomatic_range = scope:owner
			}
		}
	}
	
	# Agents
	agent_leave_threshold = 0
	agent_join_chance = {
		base = 0
	}
	valid_agent = { always = no }

	base_success_chance = {
		#Add more base success chance modifiers here later
		#All WIP

		base = 10

		# CASTER/ACTOR #
		#Number of perks in magic lifestyle
		# Wiki lies - these don't work
		# modifier = {
		# 	add = scope:owner.learning_lifestyle_perks
		# }
		
		#Magical affinity level
		modifier = {
			add = 20
			scope:owner = {
				has_trait = magical_affinity_2
			}
		}
		modifier = {
			add = 40
			scope:owner = {
				has_trait = magical_affinity_3
			}
		}


		# RECIPIENT #

		#Intelligent already
		modifier = {
			add = 20
			scope:owner = {
				has_trait = intellect_good_1
			}
		}
		modifier = {
			add = 25
			scope:owner = {
				has_trait = intellect_good_2
			}
		}
		modifier = {
			add = 30
			scope:owner = {
				has_trait = intellect_good_3
			}
		}

		#A dumbass
		modifier = {
			add = -20
			scope:owner = {
				has_trait = intellect_bad_1
			}
		}
		modifier = {
			add = -25
			scope:owner = {
				has_trait = intellect_bad_2
			}
		}
		modifier = {
			add = -30
			scope:owner = {
				has_trait = intellect_bad_3
			}
		}
	}
	base_secrecy = {
		add = secrecy_base_value
		add = countermeasure_apply_secrecy_maluses_value
	}
	
	# On Actions
	on_start = {
		scheme_owner = {
			trigger_event =  anb_enhance_ability_outcome.1
		}
	}
	on_phase_completed = {
	}
	on_hud_click  = {
	}
	on_monthly = {
		hostile_scheme_monthly_discovery_chance_effect = yes
		if = {
			limit = {
				NOT = { exists = scope:discovery_event_happening }
			}
			scheme_owner = {
				trigger_event = {
					on_action = anb_enhance_ability_ongoing
					days = { 1 7 }
				}
			}
		}
	}
	on_semiyearly = {
	}
	on_invalidated = {
		scheme_target_character = {
			save_scope_as = target
		}
		scheme_owner = {
			save_scope_as = owner
		}
		
		# Anbennar TODO: make these events into send_interface_toast
		scope:target = {
			remove_character_flag = enhance_ability_diplomacy
			remove_character_flag = enhance_ability_martial
			remove_character_flag = enhance_ability_stewardship
			remove_character_flag = enhance_ability_intrigue
			remove_character_flag = enhance_ability_learning
			remove_character_flag = enhance_ability_prowess
		}
		if = {
			limit = { scope:target = { is_alive = no } }
			scope:owner = { trigger_event = anb_spell_interruption.3 }
		}
		else_if = {
			limit = { NOT = { scope:target = { in_diplomatic_range = scope:owner } } }
			scope:owner = { trigger_event = anb_spell_interruption.1 }
			scope:target = { trigger_event = anb_spell_interruption.101 }
		}
		else_if = {
			limit = { scope:owner = { is_at_war_with = scope:target } }
			scope:owner = { trigger_event = anb_spell_interruption.6 }
			scope:target = { trigger_event = anb_spell_interruption.106 }
		}
	}
}
