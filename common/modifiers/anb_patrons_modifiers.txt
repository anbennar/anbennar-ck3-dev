﻿#############################
# Pantheonic / Regent Court #
#############################

patron_castellos_modifier = { #Increased legitimacy gain, vassals like you more
	icon = beast_positive
	legitimacy_gain_mult = 0.1
	direct_vassal_opinion = 10
}

patron_the_dame_modifier = { #Increased research, ignore negative opinions of other cultures cause you're so tolerant
	icon = magic_positive
	cultural_head_fascination_mult = 0.1
	ignore_negative_culture_opinion = yes
}

patron_esmaryal_modifier = { #Increased fertility, close family likes you more
	icon = wedding_positive
	fertility = 0.1
	close_relative_opinion = 10
}

patron_falah_modifier = { #Increased prowess, increased hunter xp gain
	icon = hunt_positive
	prowess = 2
	trait_track_hunter_xp_gain_mult = 0.1
}

patron_nerat_modifier = { #Increased learning, decreased title creation cost
	icon = learning_positive
	learning = 2
	title_creation_cost_mult = -0.1
}

patron_adean_modifier = { #Increased martial, increased knight effectiveness
	icon = tournament_positive
	martial = 2
	knight_effectiveness_mult = 0.1
}

patron_ryala_modifier = { #Increased diplomacy, characters that are attracted to you like you more
	icon = health_positive
	diplomacy = 2
	attraction_opinion = 10
}

patron_balgar_modifier = { #Increased stewardship, constructing buildings is cheaper
	icon = county_modifier_development_positive
	stewardship = 2
	build_gold_cost = -0.1
}

patron_ara_modifier = { #Increased domain taxes, increased travel safety
	icon = horse_positive
	domain_tax_mult = 0.1
	character_travel_safety_mult = 0.1
}

patron_minara_modifier = { #Personal schemes more powerful, can have one additional personal scheme
	icon = fertility_positive
	owned_personal_scheme_success_chance_add = 10
	max_personal_schemes_add = 1
}

patron_munas_modifier = { #Increased travel and naval movement speed
	icon = travel_positive
	character_travel_speed_mult = 0.1
	naval_movement_speed_mult = 0.1
}

patron_nathalyne_modifier = { #Increase intrigue, hostile schemes are more powerful
	icon = intrigue_positive
	intrigue = 2
	owned_hostile_scheme_success_chance_add = 10
}

patron_begga_modifier = { #Everyone likes you more and increased dev growth in capital cause you're so generous
	icon = economy_positive
	general_opinion = 10
	character_capital_county_monthly_development_growth_add = 0.1
}

patron_uelos_modifier = { #Increased naval movement speed, decreased embarkation cost
	icon = travel_positive
	embarkation_cost_mult = 0.1
	naval_movement_speed_mult = 0.1
}

patron_wip_modifier = { #Increased opinion, hostile schemes are less likely to succeed against you
	icon = travel_positive
	general_opinion = 10
	enemy_hostile_scheme_success_chance_add = -10
}

##################
# Alenic/Ansuwir #
##################

patron_ariadas_modifier = {
	icon = martial_positive
	martial = 2
	monthly_prestige = 1
	vassal_opinion = 5
}
patron_histra_modifier = {
	icon = fertility_positive
	fertility = 0.25
	stewardship = 2
}
patron_wulkas_modifier = {
	icon = beast_positive	
	prowess = 3
	martial = 1
}
patron_ragna_modifier = {
	icon = martial_positive
	martial = 2
	advantage = 2
}
patron_treyana_modifier = {
	icon = outdoors_positive
	forest_advantage = 2
}
patron_hyntran_modifier = {
	icon = horse_positive
	monthly_piety = 0.03
	trait_track_hunter_xp_gain_mult = 0.15
}
patron_welas_modifier = {
	icon = compass_positive
	learning = 2
	stewardship = 2
}
patron_rendan_modifier = {
	icon = magic_positive
	intrigue = 2
	# magic
	same_faith_opinion = -10
}

###########
# Lencori #
###########

patron_artanos_modifier = {
    icon = horse_positive
    martial = 2
    prowess = 2
}
patron_merisse_modifier = {
    icon = travel_positive
    movement_speed = 0.1
    character_travel_speed_mult = 0.1
    naval_movement_speed_mult = 0.1
    development_growth = 0.1
}
patron_trovecos_modifier = {
    icon = mask_positive
    learning = 2
    health = 0.25
}
patron_careslobos_modifier = {
    icon = goods_positive
    general_opinion = 10
}
patron_asmirethin_modifier = {
    icon = economy_positive
    build_gold_cost = -0.1
    build_speed = -0.1
}
patron_damarta_modifier = {
    icon = magic_positive
	monthly_prestige = 1 # TODO, this is only here while magic is being sorted out
    # magic = 3
}
patron_belouina_modifier = {
    icon = learning_positive
    monthly_prestige = 0.5
    monthly_piety = 0.5
    stress_gain_mult = -0.1
    stress_loss_mult = 0.1
}
patron_dercanos_modifier = {
    icon = feast_positive
    fertility = 0.1
    owned_personal_scheme_success_chance_add = 20
}
patron_turanos_modifier = {
    icon = martial_positive
    general_opinion = -20
    martial = 1
    diplomacy = 1
    stewardship = 1
    intrigue = 1
    learning = 1
}
patron_sorbodua_modifier = {
    icon = martial_positive
    intrigue = 1
    scheme_discovery_chance_mult = 0.05
    owned_hostile_scheme_success_chance_add = 5
}

###########################################################
# Generic Modifiers. These are used for emergent patrons. #
###########################################################

anb_martial_patron_apot_modifier = {
	icon = martial_positive
	martial = 3
}
anb_diplomacy_patron_apot_modifier = {
	icon = diplomacy_positive
	diplomacy = 3
}
anb_stewardship_patron_apot_modifier = {
	icon = stewardship_positive
	stewardship = 3
}
anb_intrigue_patron_apot_modifier = {
	icon = intrigue_positive
	intrigue = 3
}
anb_learning_patron_apot_modifier = {
	icon = learning_positive
	learning = 3
}