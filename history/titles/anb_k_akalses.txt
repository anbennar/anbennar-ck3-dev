k_akalses = {
	897.3.15 = {
		holder = araskaysit0005 #Serakh Araškaysit
	}
	913.1.12 = {
		holder = araskaysit0008 #Aruhana Araškaysit
	}
	941.4.12 = {
		holder = araskaysit0004 #Kawad VI Araškaysit
	}
	983.1.6 = {
		holder = araskaysit0003 #Kawad VII Araškaysit
	}
	987.9.3 = {
		holder = araskaysit0001 #Dariuš IV Araškaysit
	}
}

d_akalses = {
	897.3.15 = {
		holder = araskaysit0005 #Serakh Araškaysit
	}
	913.1.12 = {
		holder = araskaysit0008 #Aruhana Araškaysit
	}
	941.4.12 = {
		holder = araskaysit0004 #Kawad VI Araškaysit
	}
	983.1.6 = {
		holder = araskaysit0003 #Kawad VII Araškaysit
	}
	987.9.3 = {
		holder = araskaysit0001 #Dariuš IV Araškaysit
	}
}

c_akalses = {
	897.3.15 = {
		holder = araskaysit0005 #Serakh Araškaysit
	}
	913.1.12 = {
		holder = araskaysit0008 #Aruhana Araškaysit
	}
	941.4.12 = {
		holder = araskaysit0004 #Kawad VI Araškaysit
	}
	983.1.6 = {
		holder = araskaysit0003 #Kawad VII Araškaysit
	}
	987.9.3 = {
		holder = araskaysit0001 #Dariuš IV Araškaysit
	}
	1000.1.1 = { change_development_level = 20 }
}

c_nasilan = {
	955.8.7 = {
		holder = ruhagar0001 #Serakh Ruhagar
		liege = k_akalses
	}
	997.2.13 = { 
		holder = ruhagar0002 #Zamman Ruhagar
		liege = k_akalses
	} 
	1000.1.1 = { change_development_level = 18 }
}

c_uqlum = {
	981.3.5 = {
		holder = karyad0001 #Nuriye Karyad
		liege = k_akalses
	}

	1000.1.1 = { change_development_level = 15 }

	1020.3.16 = { 
		holder = karyad0002 #Arwia Karyiad
		liege = k_akalses
	} 
}

c_kalisad = {
	982.12.5 = {
		holder = s_ama0001 #Fereydun Šama
		liege = k_akalses
	}
	1000.1.1 = { change_development_level = 14 }
	1018.3.12 = {
		holder = s_ama0005 #Farduš Šama
		liege = k_akalses
	}
}

d_azkabar = {
	1006.6.22 = {
		liege = e_bulwar
		holder = sun_elvish0006	# Eledas I Sarelzuir
	}
}

c_azkabar = {
	1000.1.1 = { change_development_level = 20 }
}

c_kuokrumar = {
	1000.1.1 = { change_development_level = 14 }
}

c_markumar = {
	1000.1.1 = { change_development_level = 14 }
}

d_setadazar = {
	941.4.12 = {
		holder = araskaysit0004 #Kawad VI Araškaysit
	}
	983.1.6 = {
		holder = araskaysit0003 #Kawad VII Araškaysit
	}
	987.9.3 = {
		holder = araskaysit0001 #Dariuš IV Araškaysit
	}
}

c_setadazar = {
	897.3.15 = {
		holder = araskaysit0005 #Serakh Araškaysit
	}
	913.1.12 = {
		holder = araskaysit0008 #Aruhana Araškaysit
	}
	941.4.12 = {
		holder = araskaysit0004 #Kawad VI Araškaysit
	}
	983.1.6 = {
		holder = araskaysit0003 #Kawad VII Araškaysit
	}
	987.9.3 = {
		holder = araskaysit0001 #Dariuš IV Araškaysit
	}
	1000.1.1 = { change_development_level = 19 }
}

c_sarpadahna = {
	971.6.12 = {
		holder = taumakar0001 #Tigran Taumakar
		liege = k_akalses
	}
	993.12.9 = {
		holder = taumakar0003 #Tigran Taumakar
		liege = k_akalses
	}
	1000.1.1 = { change_development_level = 16 }
}

c_danaibar = {
	981.4.12 = {
		holder = farasyana0001 #Mithraš Farasyana
		liege = k_akalses
	}
	1000.1.1 = { change_development_level = 15 }
	1013.2.5 = {
		holder = farasyana0002 #Marzan Farasyana
		liege = k_akalses
	}
}

c_mitiq = {
	1000.1.1 = { change_development_level = 17 }
}

d_avamezan = {
	914.5.2 = {
		liege = k_akalses
		holder = araskaysit0006 #Ardaš Araškaysit
	}
	933.7.19 = {
		liege = k_akalses
		holder = araskaysit0007 #Andar Araškaysit
	}
	946.3.17 = {
		liege = k_akalses
		holder = araskaysit0009 #Arwia Araškaysit
	}
	971.5.12 = {
		liege = k_akalses
		holder = araskaysit0014 #Arwand Araškaysit
	}
	995.3.7 = {
		liege = k_akalses
		holder = araskaysit0017 #Širin Araškaysit
	}
}

c_erubas = {
	1000.1.1 = { change_development_level = 16 }
}

c_dazar_avamezan = {
	914.5.2 = {
		liege = k_akalses
		holder = araskaysit0006 #Ardaš Araškaysit
	}
	933.7.19 = {
		liege = k_akalses
		holder = araskaysit0007 #Andar Araškaysit
	}
	946.3.17 = {
		liege = k_akalses
		holder = araskaysit0009 #Arwia Araškaysit
	}
	971.5.12 = {
		liege = k_akalses
		holder = araskaysit0014 #Arwand Araškaysit
	}
	995.3.7 = {
		liege = k_akalses
		holder = araskaysit0017 #Širin Araškaysit
	}
	1000.1.1 = { change_development_level = 16 }
}

c_kutiriq = {
	914.5.2 = {
		liege = k_akalses
		holder = araskaysit0006 #Ardaš Araškaysit
	}
	933.7.19 = {
		liege = k_akalses
		holder = araskaysit0007 #Andar Araškaysit
	}
	946.3.17 = {
		liege = k_akalses
		holder = araskaysit0009 #Arwia Araškaysit
	}
	971.5.12 = {
		liege = k_akalses
		holder = araskaysit0014 #Arwand Araškaysit
	}
	995.3.7 = {
		liege = k_akalses
		holder = araskaysit0017 #Širin Araškaysit
	}
	1000.1.1 = { change_development_level = 12 }
}

c_nerdu_avamezan = {
	1000.1.1 = { change_development_level = 14 }
}
