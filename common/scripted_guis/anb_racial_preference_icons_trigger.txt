﻿racial_attitude_icon_neutral = {
	scope = character
	
	is_shown = {
		NOT = { has_trait_with_flag = racial_purist }
	}
}

racial_attitude_icon_purist = {
	scope = character
	
	is_shown = {
		has_trait_with_flag = racial_purist
	}
}
